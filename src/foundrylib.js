export const hookType = {
    module: {
        init: "init",
        setup: "setup",
        ready: "ready",
    },
    item: {
        preUpdate: "preUpdateItem",
        preCreate: "preCreateItem",
        preCreateOwned: "preCreateOwnedItem", // actor, item, data
    },
    canvas: { // canvas
        init: "canvasInit",
        ready: "canvasReady",
        pan: "canvasPan",
    },
    game: {
        pause: "pauseGame", // paused
    },
    token: {
        preCreate: "preCreateToken",
        create: "createToken",
        preUpdate: "preUpdateToken",
        update: "updateToken",
        preDelete: "preDeleteToken",
        delete: "deleteToken",
        paste: "pasteToken",
        control: "controlToken",
        hover: "hoverToken",
        target: "targetToken",
    },
    ui: {
        renderCompendium: "renderCompendium",
        getSceneControlButtons: "getSceneControlButtons", //controls
        collapseSceneNavigation: "collapseSceneNavigation",
        sidebarCollapse: "sidebarCollapse",
        hotbarDrop: "hotbarDrop",
        renderActoryDirectory: "renderActorDirectory", // app, html, data
    },
    chat: {
        bubble: "chatBubble",
        renderMessage: "renderChatMessage",
        message: "chatMessage",
    },
    context: {
        getSceneNavigation: "getSceneNavigationContext",
        getUserOptions: "getUserContextOptions",
        getActorDirectoryFolder: "getActorDirectoryFolderContext",
        getSceneDirectory: "getSceneDirectoryContext",
        getPlaylistDirectorySound: "getPlaylistDirectorySoundContext",
        getActorDirectoryEntry: "getActorDirectoryEntryContext", // html, options
    },
    network: {
        rtcClientConnected: "rtcClientConnected",
        rtcClientDisconnected: "rtcClientDisconnected",
        rtcSettingsChanged: "rtcSettingsChanged",
        rtcLocalStreamOpened: "rtcLocalStreamOpened",
        rtcLocalStreamClosed: "rtcLocalStreamClosed",
        rtcUserStreamChange: "rtcUserStreamChange",
        rtcCreateStreamForUser: "rtcCreateStreamForUser",
    },
};

export const scopeType = {
    world: "world",
    client: "client",
};